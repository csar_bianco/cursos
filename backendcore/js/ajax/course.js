function eliminarCurso(route,code,id,selected,type){
	var url = undefined;
	if(type=="0")
		url = window.location.href.substr(0,window.location.href.search("course"));
	else
		url = window.location.href.substr(0,window.location.href.search("lessons"));
	
	$.ajax({
		url: (url + route),
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:{"codigo":code},
		beforeSend:function(response){
			winFrame.hideFrame(id);
		},
		success:function(response){
			winFrame.hideFrame(id);
			if(response.code=="200"){		
				$("#listcrusos").DataTable().row(selected).remove().draw();
			}
		},
		error:function(response){
			winFrame.hideFrame(id);
			if(response.responseJSON.code=="404" || response.responseJSON.code=="409" || response.responseJSON.code=="500"){
				winFrame.showMessage("#modal_alerta","#advertenciamessage",response.responseJSON.response);
			}
		}
	});
}

function questions(route,questionslist){
	var url = undefined;
	url = window.location.href.substr(0,window.location.href.search("course"));
	
	$.ajax({
		url: (url + route),
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		processData: false,
		contentType: false,
		data:questionslist,
		beforeSend:function(response){
			winFrame.showForm("#modal_wait");
		},
		success:function(response){
			//winFrame.hideFrame("#modal_wait");
			if(response.code=="200"){		
				console.log(response);
			}
		},
		error:function(response){
			winFrame.hideFrame("#modal_wait");
			if(response.responseJSON.code=="404" || response.responseJSON.code=="409" || response.responseJSON.code=="500"){
				winFrame.showMessage("#modal_alerta","#advertenciamessage",response.responseJSON.response);
			}
		}
	});
}

function viewcourseasociated(route,id,code,coursesasociated){
	var url = undefined;
	url = window.location.href.substr(0,window.location.href.search("course"));
	
	$.ajax({
		url: (url + route),
		headers:{
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"post",
		data:{"codigo":code},
		beforeSend:function(response){
			winFrame.hideFrame(id);
		},
		success:function(response){
			winFrame.hideFrame(id);
			if(response.code==200){		
				if(response.response.length >= 2){
					winFrame.showMessage("#modal_alerta","#advertenciamessage","Hemos encontrado lecciones asociadas");
					$("#rowlecciones").removeClass("ocultar");
					$("#rowlecciones").addClass("mostrar");

					$("#rowtablelecciones").removeClass("ocultar");
					$("#rowtablelecciones").addClass("mostrar");

					$("html, body").animate({ scrollTop: $("#bloquelecciones").offset().top }, 1500);
					//TABLA DE LECCIONES
					$("#titlecourse").text(response.response[0].course_name);
					$("#listcourseasociated").DataTable().rows().remove().draw(false);
					for(i = 1; i < response.response.length; i++){
						$("#listcourseasociated").DataTable().row.add( [
							'<div class="row">' + 
								'<div class="col-xs-12 item-header flex-center">' +
									'<a href="' + (url + response.response[i].course_route) + '" class="" data-lightbox="roadtrip">' +
										'<img src="' + (url + response.response[i].course_route) + '" class="img-responsive img-thumbnail fixed-image"/>' +
									'</a>' +
								'</div>' +
							'</div>',
							response.response[i].course_name,
							'<div class="row">' +
								'<div class="col-xs-12 col-sm-12 col-md-12 flex-center">' +
									'<button type="button" id="btnSeleccionarLeccion" class="lessonsselections' + response.response[i].id + ' btn btn-default" data-codigo="' + btoa(response.response[i].id) + '">' +
										'<i class="fa fa-check" aria-hidden="true"></i> SELECCIONAR' +
									'</button>' +
								'</div>' +
							'</div>',
						] ).draw( false );
					}
				}
			}
		},
		error:function(response){
			winFrame.hideFrame(id);
			if(response.status==404 || response.status==409 || response.status==500){
				if(response.status==404){//SI NO POSEE CURSOS/LECCIONES ASOCIADAS
					$("html, body").animate({ scrollTop: $("#rowespecificaciones").offset().top }, 1500);

					$("#rowlecciones").removeClass("mostrar");
					$("#rowlecciones").addClass("ocultar");

					$("#rowtablelecciones").removeClass("mostrar");
					$("#rowtablelecciones").addClass("ocultar");
				}

				winFrame.showMessage("#modal_alerta","#advertenciamessage",response.responseJSON.response);
			}
		}
	});
}