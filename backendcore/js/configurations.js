$(document).ready(function(){
	$("#descripcion").summernote({
		lang: 'es-ES',
		toolbar: [
			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough', 'superscript', 'subscript']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
		],
		fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '32'],
		height: 300,
		minHeight: null,
		maxHeight: null,
		callbacks: {
			onChange:function(event){},
			onFocus: function() {},
			onKeyup: function(e) {},
			onPaste: function(e){
				//e.originalEvent.clipboardData.getData('text')
				var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('text');
				e.preventDefault();
				setTimeout(function () {
					document.execCommand('insertText', false, bufferText);                    
				}, 10);						
			}
		},
		placeholder:"Ingrese la descripción asociada"
	});

	var dateformat = "yy/mm/dd";
	$("#fechainicio").datepicker({ minDate: 0, maxDate: "+12M +0D" });
	$("#fechainicio").datepicker( "option", "dateFormat", dateformat, $.datepicker.regional[ "es" ]);
	
	$("#fechafin").datepicker({ minDate: 1, maxDate: "+12M +0D" });
	$("#fechafin").datepicker( "option", "dateFormat", dateformat, $.datepicker.regional[ "es" ] );
});