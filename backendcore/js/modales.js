var winFrame = {
	showMessage:function(id,idmessage,message){
		$(idmessage).empty();
		$(idmessage).append(message);
		$(id).modal("show");
	},
	hideFrame:function(id){
		$(id).modal("hide");
	},
	showForm:function(id){
		$(id).modal("show");
	}
};