var Validations = {
	isEmpty:function(id){
		if($(id).val().length==0){
			return 1;
		}else{
			return 0;
		}
	},
	isNull:function(id){
		if(atob($(id).val())=="2"){
			return 1;
		}else{
			return 0;
		}
	},
	input:function(id,idbloque,iderror){
		$(id).bind("input",function(){
			if($(id).val().length > 3){
				$(idbloque).removeClass("has-error");
				$(idbloque).addClass("has-feedback");
				$(iderror).empty();
			}
		});
	},
	withoutpicture:function(pic){
		if(pic==undefined){
			return 1;
		}else{
			return 0;
		}
	},
	cleanInput:function(id){
		$(id).val("");
	},
	cleanSelect:function(id){
		$(id).val(btoa("2"));
	}
};