$(document).ready(function(){
	lightbox.option({
		'resizeDuration': 200,
		'wrapAround': true,
		'showImageNumberLabel':false,
	});

	var tablehistorial = $("#listparentscourse").DataTable({
		"searching": true,
		"bInfo": false,
		"bSort":true,
		"Info":false,
		"bLengthChange": false,
		"iDisplayLength": 4,
		"order": [[ 0, "asc" ]],
		"language": {
		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ registros",
		"sZeroRecords":    "No se encontraron pedidos registrados en el sistema.",
		"sEmptyTable":     "No posee lecciones registrados en el sistema.",
		"sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
		"sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
		"sFirst":    "Primero",
		"sLast":     "Último",
		"sNext":     "Siguiente",
		"sPrevious": "Anterior"
		},
		"oAria": {
		"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		}
		},

		"fixedHeader": {
		"header": true,
		"footer": true
		},
		"bPaginate": true,
		"responsive":true,
		"bAutoWidth": false,
		"dom": '<"top"p>rt<"bottom"fl><"clear">',
	});

	tablehistorial = $("#listcursosasociated").DataTable({
		"searching": true,
		"bInfo": false,
		"bSort":true,
		"Info":false,
		"bLengthChange": false,
		"iDisplayLength": 6,
		"order": [[ 0, "asc" ]],
		"language": {
		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ registros",
		"sZeroRecords":    "No se encontraron pedidos registrados en el sistema.",
		"sEmptyTable":     "No posee lecciones registrados en el sistema.",
		"sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
		"sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
		"sFirst":    "Primero",
		"sLast":     "Último",
		"sNext":     "Siguiente",
		"sPrevious": "Anterior"
		},
		"oAria": {
		"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		}
		},

		"fixedHeader": {
		"header": true,
		"footer": true
		},
		"bPaginate": true,
		"responsive":true,
		"bAutoWidth": false,
		"dom": '<"top"p>rt<"bottom"fl><"clear">',
	});
	
	var code = undefined;
	var lessons = [];
	var parentscourse = undefined;

	$(document).on("click","#btnSeleccionar",function(){
		if(parentscourse!=undefined){
			winFrame.showMessage("#modal_alerta","#advertenciamessage","Ya ha seleccionado un curso.")
		}else{
			parentscourse = $(this).parents("tr").data("parentscourse");
			$(".selections" + atob(parentscourse)).prop("disabled",true);
		}
	});

	$(document).on("click","#btnAsociar",function(){
		if(parentscourse==undefined){
			winFrame.showMessage("#modal_alerta","#advertenciamessage","Debe seleccionar un curso antes de asociar.")
		}else{
			code = $(this).parents("tr").data("codigo");
			$(".asociado" + atob(code)).prop("disabled",true);
			lessons.push('{"parents":"' + parentscourse + '","child":"' + code + '"}');
		}
	});
	
	//GUARDO UNA ASOCIACIÓN
	$("#btnGuardar").on("click",function(){
		if(lessons.length == 0){
			winFrame.showMessage("#modal_alerta","#advertenciamessage","Seleccione un curso y una lección antes de continuar.")
		}else{
			$("#asociados").attr("value",JSON.stringify(lessons));
			$("#form-asociar").submit();
		}
	});
	
	//SE REMUEVE UNA ASOCIACIÓN HECHA
	$("#btnCancelar").on("click",function(){
		winFrame.showMessage("#modal_confirmar_asociated","#list_asociatedcourse","¿Está usted seguro de remover su asociación?")
	});

	$("#removerasociated").on("click",function(){
		window.location.reload();
	});

	//IMPRIMO EL MENSAJE DEL SERVIDOR
	if($("#resultado").val()!=undefined){
		winFrame.showMessage("#modal_alerta","#advertenciamessage",$("#resultado").val());
	}
});