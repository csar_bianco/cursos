$(document).ready(function(){
	lightbox.option({
		'resizeDuration': 200,
		'wrapAround': true,
		'showImageNumberLabel':false,
	});

	
	var tablehistorial = $("#listusers").DataTable({
		"searching": true,
		"bInfo": false,
		"bSort":true,
		"Info":false,
		"bLengthChange": false,
		"iDisplayLength": 6,
		"order": [[ 0, "asc" ]],
		"language": {
		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ registros",
		"sZeroRecords":    "No se encontraron usuarios registrados en el sistema.",
		"sEmptyTable":     "No posee usuarios registrados en el sistema.",
		"sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
		"sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
		"sFirst":    "Primero",
		"sLast":     "Último",
		"sNext":     "Siguiente",
		"sPrevious": "Anterior"
		},
		"oAria": {
		"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		}
		},

		"fixedHeader": {
		"header": true,
		"footer": true
		},
		"bPaginate": true,
		"responsive":true,
		"bAutoWidth": false,
		"dom": '<"top"p>rt<"bottom"fl><"clear">',
	});

	tablehistorial = $("#listcrusosasignar").DataTable({
		"searching": true,
		"bInfo": false,
		"bSort":true,
		"Info":false,
		"bLengthChange": false,
		"iDisplayLength": 6,
		"order": [[ 0, "asc" ]],
		"language": {
		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ registros",
		"sZeroRecords":    "No se encontraron cursos registrados en el sistema.",
		"sEmptyTable":     "No posee cursos registrados en el sistema.",
		"sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
		"sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
		"sFirst":    "Primero",
		"sLast":     "Último",
		"sNext":     "Siguiente",
		"sPrevious": "Anterior"
		},
		"oAria": {
		"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		}
		},

		"fixedHeader": {
		"header": true,
		"footer": true
		},
		"bPaginate": true,
		"responsive":true,
		"bAutoWidth": false,
		"dom": '<"top"p>rt<"bottom"fl><"clear">',
	});
	
	var code = undefined;
	var lessons = [];
	var asociatedusers = undefined;

	//SELECCIONO UN USUARIO
	$(document).on("click","#btnSeleccionar",function(){
		if(asociatedusers!=undefined){
			winFrame.showMessage("#modal_alerta","#advertenciamessage","Ya ha seleccionado a un usuario.")
		}else{
			asociatedusers = $(this).parents("tr").data("asociatedusers");
			$(".selections" + atob(asociatedusers)).prop("disabled",true);
		}
	});

	//ASIGNO UN CURSO
	$(document).on("click","#btnAsociar",function(){
		if(asociatedusers==undefined){
			winFrame.showMessage("#modal_alerta","#advertenciamessage","Debe seleccionar a un usuario antes de continuar.")
		}else{
			code = $(this).parents("tr").data("codigo");
			$(".asociado" + atob(code)).prop("disabled",true);
			lessons.push('{"user":"' + asociatedusers + '","course":"' + code + '"}');
		}
	});
	
	//GUARDO UNA ASIGNACIÓN
	$("#btnGuardar").on("click",function(){
		if(lessons.length == 0){
			winFrame.showMessage("#modal_alerta","#advertenciamessage","Seleccione a un usuario y un curso antes de continuar.")
		}else{
			$("#asociados").attr("value",JSON.stringify(lessons));
			$("#form-asociar").submit();
		}
	});
	
	//SE REMUEVE UNA ASOCIACIÓN HECHA
	$("#btnCancelar").on("click",function(){
		winFrame.showMessage("#modal_confirmar_asociated","#list_asociatedcourse","¿Está usted seguro de remover su asociación?")
	});

	$("#removerasociated").on("click",function(){
		window.location.reload();
	});

	//IMPRIMO EL MENSAJE DEL SERVIDOR
	if($("#resultado").val()!=undefined){
		winFrame.showMessage("#modal_alerta","#advertenciamessage",$("#resultado").val());
	}
});