var pic = undefined;

function openFile(event){
  pic = event;
}


$(document).ready(function(){
    var tablehistorial = $("#listcrusos").DataTable({
		"searching": true,
		"bInfo": false,
		"bSort":true,
		"Info":false,
		"bLengthChange": false,
		"iDisplayLength": 4,
		"order": [[ 0, "asc" ]],
		"language": {
		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ registros",
		"sZeroRecords":    "No se encontraron pedidos registrados en el sistema.",
		"sEmptyTable":     "No posee cursos registrados en el sistema.",
		"sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
		"sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
		"sFirst":    "Primero",
		"sLast":     "Último",
		"sNext":     "Siguiente",
		"sPrevious": "Anterior"
		},
		"oAria": {
		"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		}
		},

		"fixedHeader": {
		"header": true,
		"footer": true
		},
		"bPaginate": true,
		"responsive":true,
		"bAutoWidth": false,
		"dom": '<"top"p>rt<"bottom"fl><"clear">',
	});

	var tablequestions = $("#listcourseasociated").DataTable({
		"searching": true,
		"bInfo": false,
		"bSort":true,
		"Info":false,
		"bLengthChange": false,
		"iDisplayLength": 6,
		"order": [[ 0, "asc" ]],
		"language": {
		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ registros",
		"sZeroRecords":    "No se encontraron pedidos registrados en el sistema.",
		"sEmptyTable":     "No posee cursos registrados en el sistema.",
		"sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
		"sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
		"sFirst":    "Primero",
		"sLast":     "Último",
		"sNext":     "Siguiente",
		"sPrevious": "Anterior"
		},
		"oAria": {
		"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		}
		},

		"fixedHeader": {
		"header": true,
		"footer": true
		},
		"bPaginate": true,
		"responsive":true,
		"bAutoWidth": false,
		"dom": '<"top"p>rt<"bottom"fl><"clear">',
	});

	var courseasociated = [];
	$("#confirmarasignated").on("click",function(){
		if(code!=undefined){
			viewcourseasociated("course/viewcourseasociated","#modal_confirmar_asignated",code,courseasociated);
		}
	});

	//SELECCIONE LA LECCIÓN
	var codelessons = undefined;
	$(document).on("click","#btnSeleccionarLeccion",function(){
		if(codelessons!=undefined){
			winFrame.showMessage("#modal_alerta","#advertenciamessage","Ya ha seleccionado una lección, cancele e intente nuevamente.")
		}else{
			codelessons = $(this).data("codigo");
			$(".lessonsselections" + atob(codelessons)).prop("disabled",true);
		}
	});

	//SELECCIONO EL CURSO
	var code = undefined;
	$(document).on("click","#btnSeleccionarCourse",function(){
		if(code!=undefined){
			winFrame.showMessage("#modal_alerta","#advertenciamessage","Ya ha seleccionado un curso, cancele e intente nuevamente.")
		}else{
			code = $(this).parents("tr").data("codigo");
			$(".selectioncourse" + atob(code)).prop("disabled",true);
			winFrame.showMessage("#modal_confirmar_asignated","#list_asociatedcourse","¿Está usted seguro de seleccionar este curso?")
		}
	});

	//AÑADO OTRA PREGUNTA
	$("#btnsinewquestions").on("click",function(){
		//CLEAN INPUTS
		var innerquestions = localStorage.getItem("questionsgeneral");
		Validations.cleanInput("#titulopregunta");
		Validations.cleanSelect("#tipopregunta");
		$("#descripcion").summernote("code","");
		$("#descripcion").summernote("disable");
		$("#imagenreferencial").attr("disabled",true);
		pic = undefined;
		delete localStorage.questionsgeneral;
	});

	//ENVIO EL CUESTIONARIO ACTUAL
	$("#btnNonewquestions").on("click",function(){
		if(courseasociated.length > 0){
			var formdata = new FormData();
			var imagenproducto = $("#imagenreferencial")[0].files[0];

			formdata.append("questions",courseasociated);
			formdata.append("picture",imagenproducto);
			formdata.append("titlequestion",$("#titulopregunta").val());
			formdata.append("typequestions",$("#tipopregunta").val());
			formdata.append("descripcionquestion",$("#descripcion").summernote("code"));
			questions("course/questions",formdata);
		}
	});


	//GUARDO LA PREGUNTA
	delete localStorage.questionsgeneral;
	$("#btnGuardar").on("click",function(){
		if(Validations.isEmpty("#titulopregunta") && Validations.withoutpicture(pic)){
			winFrame.showMessage("#modal_alerta","#advertenciamessage","Todos los campos son obligatorios.")
		}else if(Validations.isEmpty("#titulopregunta")){
			winFrame.showMessage("#modal_alerta","#advertenciamessage","El título de la pregunta es obligatorio")
		}else if(Validations.withoutpicture(pic)){
			winFrame.showMessage("#modal_alerta","#advertenciamessage","El archivo media es obligatorio")
		}else{
			if(codelessons==undefined){//CURSO SIN LECCIONES ASOCIADAS
				courseasociated.push('{"course":"' + code + '","listquestions":[{"id":"' + btoa(0) + '","title":"' + $("#titulopregunta").val() + '","questions":"' + btoa($("#descripcion").summernote("code")) +'"}]}');	
			}else{
				courseasociated.push('{"course":"' + codelessons + '","listquestions":[{"id":"' + btoa(code) + '","title":"' + $("#titulopregunta").val() + '","questions":"' + btoa($("#descripcion").summernote("code")) +'"}]}');	
			}

			if(localStorage.getItem("questionsgeneral")==null){
				winFrame.showMessage("#modal_confirmar_newquestion","#list_couresenewquestions","¿Desea añadir otra pregunta?");
				localStorage.setItem("questionsgeneral",courseasociated);
			}
		}
	});

	//SETUP IMAGEN
	var _URL = window.URL || window.webkitURL;
	$("#imagenreferencial").bind("change",function(){//CARGO LA PRIMERA IMAGEN
		if ((file = this.files[0])) {
			img = new Image();
			img.onload = function(){}
	

			img.onerror = function() {
				//$("#avataradvertencia").text("Extensiones permitidas jpg, png");
				//$("#alerta_avatar").modal("show");
			};
			
			img.src = _URL.createObjectURL(file);
            if((file.size/1024) > 30000){
				winFrame.showMessage("#modal_alerta","#advertenciamessage","<b>El archivo ha utilizar no puede exceder los 3MB.</b>");
                pic = undefined;
            }else{
                if(file.type!="image/jpeg" && file.type!="image/png" && file.type!="image/jpg" && file.type!="video/*"){
					winFrame.showMessage("#modal_alerta","#advertenciamessage","<b>Extensión del archivo no permitida.</b>");
                    pic = undefined;
				}
            }
		}
	});

	//IMPRIMO EL MENSAJE DEL SERVIDOR
	if($("#resultado").val()!=undefined){
		winFrame.showMessage("#modal_alerta","#advertenciamessage",$("#resultado").val());
	}

	$("#descripcion").summernote("disable");
	$("#tipopregunta").on("change",function(){
		if(Validations.isNull($(this))){
			winFrame.showMessage("#modal_alerta","#advertenciamessage","Seleccione un tipo de pregunta válido");
			$("#descripcion").summernote("disable");
			$("#imagenreferencial").attr("disabled",true);
		}else{
			if(atob($(this).val())==1){//IMAGEN
				$("#descripcion").summernote("disable");
				$("#imagenreferencial").attr("disabled",false);
			}else{//TEXTO & IMAGEN
				$("#descripcion").summernote("enable");
				$("#imagenreferencial").attr("disabled",false);
			}
		}
	});

	//CANCELAR OPCIONES
	$("#btnCancelar").on("click",function(){
		window.location.reload();
	});

	//CANCELAR LA SECCIÓN DE UN CURSO
	$("#salircourseasignated").on("click",function(){
		$(".selectioncourse" + atob(code)).prop("disabled",false);
		code = undefined;
	});
});