var pic = undefined;

function openFile(event){
  pic = event;
}

$(document).ready(function(){
	//SETUP IMAGEN
	var _URL = window.URL || window.webkitURL;
	$("#imagenreferencial").bind("change",function(){//CARGO LA PRIMERA IMAGEN
		if ((file = this.files[0])) {
			img = new Image();
			img.onload = function(){
				if((img.width != 128) && (img.height !=128) || (img.width == 128) && (img.height !=128)){
					winFrame.showMessage("#modal_alerta","#advertenciamessage","<b>La imagen no cumple con las medidas establecidas.</b>");
					pic = undefined;
				}
			}
	

			img.onerror = function() {
				//$("#avataradvertencia").text("Extensiones permitidas jpg, png");
				//$("#alerta_avatar").modal("show");
			};
			
			img.src = _URL.createObjectURL(file);
            if((file.size/1024) > 30000){
				winFrame.showMessage("#modal_alerta","#advertenciamessage","<b>El archivo ha utilizar no puede exceder los 3MB.</b>");
                pic = undefined;
            }else{
                if(file.type!="image/jpeg" && file.type!="image/png" && file.type!="image/jpg"){
					winFrame.showMessage("#modal_alerta","#advertenciamessage","<b>Extensión del archivo no permitida.</b>");
                    pic = undefined;
                }
            }
		}
	});
	
	//SUBMIT: VALIDANDO LOS CAMPOS EN EL FORMULARIO
	$("#btnGuardar").on("click",function(event){
		if(Validations.isEmpty("#nombrecurso") && Validations.isEmpty("#fechainicio") && Validations.isNull("#estadocurso") && pic==undefined && Validations.isEmpty("#fechafin")){
			winFrame.showMessage("#modal_alerta","#advertenciamessage","Todos los campos son obligatorios.");
		}else if(Validations.isEmpty("#nombrecurso")){
			winFrame.showMessage("#modal_alerta","#advertenciamessage","El nombre de su curso es obligatorio.");
		}else if(pic==undefined){
			winFrame.showMessage("#modal_alerta","#advertenciamessage","El imagen asociada a su curso es obligatoria.");
		}else if(Validations.isEmpty("#fechainicio")){
			winFrame.showMessage("#modal_alerta","#advertenciamessage","El fecha de inicio es obligatoria.");
		}else if(Validations.isEmpty("#fechafin")){
			winFrame.showMessage("#modal_alerta","#advertenciamessage","El fecha de fin es obligatoria.");
		}else if(atob($("#estadocurso").val())=="2"){
			winFrame.showMessage("#modal_alerta","#advertenciamessage","El estado de su curso es obligatorio.");
		}else{
			$("#form-cursos").submit();
		}
	});
	
	Validations.input("#nombrecurso","#bloquecursonombre","#errormessage_name");
	
	//IMPRIMO EL MENSAJE DEL SERVIDOR
	if($("#resultado").val()!=undefined){
		winFrame.showMessage("#modal_alerta","#advertenciamessage",$("#resultado").val());
	}
});