$(document).ready(function(){
	lightbox.option({
		'resizeDuration': 200,
		'wrapAround': true,
		'showImageNumberLabel':false,
	});

	
	var tablehistorial = $("#listcrusos").DataTable({
		"searching": true,
		"bInfo": false,
		"bSort":true,
		"Info":false,
		"bLengthChange": false,
		"iDisplayLength": 6,
		"order": [[ 0, "asc" ]],
		"language": {
		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_ registros",
		"sZeroRecords":    "No se encontraron pedidos registrados en el sistema.",
		"sEmptyTable":     "No posee cursos registrados en el sistema.",
		"sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
		"sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
		"sFirst":    "Primero",
		"sLast":     "Último",
		"sNext":     "Siguiente",
		"sPrevious": "Anterior"
		},
		"oAria": {
		"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
		"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		}
		},

		"fixedHeader": {
		"header": true,
		"footer": true
		},
		"bPaginate": true,
		"responsive":true,
		"bAutoWidth": false,
		"dom": '<"top"p>rt<"bottom"fl><"clear">',
	});
	
	var code = undefined;
	var selected = undefined;
	var type = undefined;
	$("#removercurso").on("click",function(){
		if(code!=undefined){
			var route = "";
			if(type=="0"){
				route = "course/delete";
			}else{
				route = "lessons/delete";
			}
			
			eliminarCurso(route,code,"#modal_confirmar_remover",selected,type);
		}
	});
	
	$(document).on("click","#btneliminar",function(){
		code = $(this).parents("tr").data("codigo");
		type = atob($(this).parents("tr").data("tipo"));
		selected = $(this).parents("tr");
		$("#list_advertenciacurso").text("¿Está usted seguro de remover este curso?");
		$("#modal_confirmar_remover").modal("show");
	});
});