<?php
	return [
		"general_error" => "Hubo un problema en su registro, intente nuevamente.",
		"match_name" => "El nombre ha utilizar ya se encuentra en uso.",
		"match_asociated" => "El curso ya se encuentra asociado.",
		"match_course_delete" => "Imposible remover, su curso se encuentra asociado.",
		"match_asignated_user" => "El usuario ya posee ese curso asignado.",
		"not_course_asociated" => "No posee lecciones asociadas"
	];
?>