<?php
	return [
		"yes_register" => "Los cambios han sido guardados satisfactoriamente.",
		"yes_asociated" => "La asociación ha sido guardada satisfactoriamente.",
		"yes_asignated" => "La asignación ha sido guardada satisfactoriamente.",
		"yes_delete_curso" => "El curso ha sido removido satisfactoriamente."
	];
?>