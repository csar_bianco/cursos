@extends("layout.dashboard")
@section("mi-scripts")
<script src="{{asset('js/configurations.js')}}"></script>
<script src="{{asset('js/validaciones.js')}}"></script>
<script src="{{asset('js/modales.js')}}"></script>
<script src="{{asset('js/course/asignated.js')}}"></script>
@endsection
@section("section-body-course")
	@if(Session::has("resultado"))
		<input type="hidden" id="resultado" name="resultado" value="{{Session::get('resultado')}}"/>
	@endif
	<form id="form-asociar" class="form-horizontal" action="{{url('course/asignated')}}" accept-charset="UTF-8" enctype="multipart/form-data" method="post">
		<input type="hidden" id="_token" name="_token" value="{{csrf_token()}}"/>
		<input type="hidden" id="asociados" name="asociados"/>
		<div class="form-group">
			<div class="col-xs-12 col-md-12 is-empty text-left">
				<label for="">USUARIOS EXISTENTES</label>
			</div>
		</div>
		<div class="form-group">
            @if(count($users)==0)
                <div class="col-xs-12 col-md-12 is-empty text-right">
				    <label for="">NO HAY USUARIOS REGISTRADOS EN EL SISTEMA</label>
			    </div>
            @else
                <div id="bloquecursos" class="col-xs-12 col-sm-12 col-md-12 has-feedback">
					<table id="listusers" class="table table-bordered">
						<thead>
							<tr>
								<th>NOBMRE</th>
								<th>EMAIL</th>
								<th>ASIGNAR</th>
							</tr>
						</thead>
						<tbody>
							@foreach($users as $u)
								<tr data-asociatedusers="{{e(base64_encode($u->id))}}">
									<td>{{$u->user_name}}</td>
									<td>{{$u->user_email}}</td>
									<td>
										<div class="row">
											<div class="col-xs-12 col-sm-12 col-md-12 flex-center">
												<button type="button" id="btnSeleccionar" class="btn btn-default selections{{e($u->id)}}">
													<i class="fa fa-check" aria-hidden="true"></i> SELECCIONAR
												</button>
											</div>
										</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
                </div>
            @endif
		</div>
		<hr/>
		<div id="bloquetexto" class="form-group">
			<div class="col-xs-12 col-md-12 is-empty text-center">
				<label for="">CURSOS EXISTENTES</label>
			</div>
		</div>
		<hr/>
		<div class="form-group">
			<div class="col-xs-12">
				<table id="listcrusosasignar" class="table table-bordered">
					<thead>
						<tr>
							<th>IMAGEN</th>
							<th>NOMBRE</th>
							<th>ASOCIAR</th>
						</tr>
					</thead>
					<tbody>
						@foreach($list as $l)
							<tr data-codigo="{{e(base64_encode($l->id))}}">
								<td>
									<div class="row">
										<div class="col-xs-12 item-header flex-center">
											<a href="{{url('/').'/'.$l->course_route}}" class="" data-lightbox="roadtrip">
												<img src="{{url('/').'/'.$l->course_route}}" class="img-responsive img-thumbnail fixed-image"/>
											</a>
										</div>
									</div>
								</td>
								<td>{{$l->course_name}}</td>
								<td>
									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 flex-center">
											<button type="button" id="btnAsociar" class="btn btn-default asociado{{e($l->id)}}">
												<i class="fa fa-plus" aria-hidden="true"></i> ASOCIAR
											</button>
										</div>
									</div>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		<hr class="hr-cursos"/>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 botonera">
				<button id="btnGuardar" type="button" class="btn btn-default pull-left">
					<i class="fa fa-save" aria-hidden="true"></i> GUARDAR
				</button>
				<button id="btnCancelar" type="button" class="btn btn-default pull-left">
					<i class="fa fa-times" aria-hidden="true"></i> CANCELAR
				</button>
				<a id="btnAdministrar" href="{{url('course/course')}}" class="btn btn-default pull-left">
					<i class="fa fa-undo" aria-hidden="true"></i> REGRESAR
				</a>
				<a id="btnAdministrar" href="{{url('course/list/users')}}" class="btn btn-default pull-right">
					<i class="fa fa-cog" aria-hidden="true"></i> ADMINISTRAR
				</a>
			</div>
		</div>
	</form>
	<hr/>
	@include("modales.alerta")
	@include("modales.confirmar_asociated")
@endsection