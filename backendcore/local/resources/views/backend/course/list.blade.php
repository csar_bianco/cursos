@extends("layout.dashboard")
@section("mi-scripts")
<script src="{{asset('js/configurations.js')}}"></script>
<script src="{{asset('js/validaciones.js')}}"></script>
<script src="{{asset('js/modales.js')}}"></script>
<script src="{{asset('js/ajax/course.js')}}"></script>
<script src="{{asset('js/course/list.js')}}"></script>
@endsection
@section("section-body-course")
	@if(Session::has("resultado"))
		<input type="hidden" id="resultado" name="resultado" value="{{Session::get('resultado')}}"/>
	@endif
	<div class="form-group">
		<div class="col-xs-12 col-md-12 is-empty text-center">
			@if($type=="0")
				<label for="">CURSOS EXISTENTES</label>
			@else
				<label for="">LECCIONES EXISTENTES</label>
			@endif
		</div>
	</div>
	<hr/>
	<div class="row">
		<div class="col-xs-12">
			<table id="listcrusos" class="table table-bordered">
				<thead>
					<tr>
						<th>IMAGEN</th>
						<th>NOMBRE</th>
						<th>MODIFICAR</th>
						<th>REMOVER</th>
					</tr>
				</thead>
				<tbody>
					@foreach($list as $l)
						<tr data-codigo="{{e(base64_encode($l->id))}}" data-tipo="{{e(base64_encode($type))}}">
							<td>
								<div class="row">
									<div class="col-xs-12 item-header flex-center">
										<a href="{{url('/').'/'.$l->course_route}}" class="" data-lightbox="roadtrip">
											<img src="{{url('/').'/'.$l->course_route}}" class="img-responsive img-thumbnail fixed-image"/>
										</a>
									</div>
								</div>
							</td>
							<td>{{$l->course_name}}</td>
							<td>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 flex-center">
										<button type="button" id="btnModificar" class="btn btn-default"> 
											<i class="fa fa-edit" aria-hidden="true"></i> MODIFICAR
										</button>
									</div>
								</div>
							</td>
							<td>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 flex-center">
										<button type="button" id="btneliminar" class="btn btn-default">
											<i class="fa fa-trash" aria-hidden="true"></i> REMOVER
										</button>
									</div>
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
	<hr/>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<a id="btnAdministrar" href="{{url('course/asociated')}}" class="btn btn-default pull-left">
				<i class="fa fa-book" aria-hidden="true"></i> ASOCIAR
			</a>
			@if($type=="0")
				<a id="btnAdministrar" href="{{url('course/course')}}" class="btn btn-default pull-right">
					<i class="fa fa-undo" aria-hidden="true"></i> REGRESAR
				</a>
			@else
				<a id="btnAdministrar" href="{{url('course/lessons')}}" class="btn btn-default pull-right">
					<i class="fa fa-undo" aria-hidden="true"></i> REGRESAR
				</a>
			@endif
		</div>
	</div>
	<hr/>
	@include("modales.alerta")
	@include("modales.confirmar")
@endsection