@extends("layout.dashboard")
@section("mi-scripts")
<script src="{{asset('js/configurations.js')}}"></script>
<script src="{{asset('js/validaciones.js')}}"></script>
<script src="{{asset('js/modales.js')}}"></script>
<script src="{{asset('js/ajax/course.js')}}"></script>
<script src="{{asset('js/course/questions.js')}}"></script>
@endsection
@section("section-body-course")
	@if(Session::has("resultado"))
		<input type="hidden" id="resultado" name="resultado" value="{{Session::get('resultado')}}"/>
	@endif
    <form id="form-cursos" class="form-horizontal" action="{{url('course/questions')}}" accept-charset="UTF-8" enctype="multipart/form-data" method="post">
        <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}"/>
        <div id="bloquetexto" class="form-group">
            <div class="col-xs-12 col-md-12 is-empty text-center">
                <label for="" class="">DESCRIBA SU PREGUNTA</label>
            </div>
        </div>
		<hr/> 
        <div id="bloquetexto" class="form-group">
            <div class="col-xs-12 col-md-12 is-empty text-left">
                <label for="" class="">INSTRUCCIONES</label>
            </div>
        </div>      
        <div id="bloquetexto" class="form-group">
            <div class="col-xs-12 col-md-12 is-empty text-left">
                <span for="" class="">- Seleccione el curso de su pregunta</span>
            </div>
            <div class="col-xs-12 col-md-12 is-empty text-left">
                <span for="" class="">- Si su curso tiene una asociación deberá seleccionar la lección asociada</span>
            </div>
            <div class="col-xs-12 col-md-12 is-empty text-left">
                <span for="" class="">- Ingrese la información descripta/solicitada</span>
            </div>
            <div class="col-xs-12 col-md-12 is-empty text-left">
                <span for="" class="">- Haga click en Guardar para continuar</span>
            </div>
            <div class="col-xs-12 col-md-12 is-empty text-left">
                <span for="" class="">- Una vez hecho click podrá guardar la preguntar o añadir otra</span>
            </div>
            <hr/>
            <div class="col-xs-12 col-md-12 is-empty text-left">
                <span for="" class=""><strong>Haga click para re-iniciar sus opciones</strong></span>
            </div>
        </div>    
        <hr/>   
        <div class="form-group">
            <div class="col-xs-12">
                <table id="listcrusos" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>IMAGEN</th>
                            <th>NOMBRE</th>
                            <th>ASOCIAR</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($list as $l)
                            <tr data-codigo="{{e(base64_encode($l->id))}}">
                                <td>
                                    <div class="row">
                                        <div class="col-xs-12 item-header flex-center">
                                            <a href="{{url('/').'/'.$l->course_route}}" class="" data-lightbox="roadtrip">
                                                <img src="{{url('/').'/'.$l->course_route}}" class="img-responsive img-thumbnail fixed-image"/>
                                            </a>
                                        </div>
                                    </div>
                                </td>
                                <td>{{$l->course_name}}</td>
                                <td>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 flex-center">
                                            <button type="button" id="btnSeleccionarCourse" class="btn btn-default selectioncourse{{e($l->id)}}"> 
                                                <i class="fa fa-plus" aria-hidden="true"></i> ASOCIAR
                                            </button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <hr/>
        <div id="rowlecciones" class="form-group ocultar">
			<div id="bloquelecciones" class="col-xs-12 col-sm-12 col-md-12 has-feedback">
                <label id="titlecourse" for="" class=""></label>
            </div>
            <div class="col-xs-12 col-md-12 is-empty text-right">
                <label for="" class="">Lecciones Asociadas</label>
            </div>
        </div>
        <div id="rowtablelecciones" class="form-group ocultar">
            <div class="col-xs-12">
                <table id="listcourseasociated" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>IMAGEN</th>
                            <th>NOMBRE</th>
                            <th>SELECCIONAR</th>
                        </tr>
                    </thead>
                    </tbody> 

                    </tbody>
                </table>
            </div>
        </div>
        <hr/>
        <div id="rowespecificaciones" class="form-group">
			<div id="bloquecursonombre" class="col-xs-12 col-sm-12 col-md-6 has-feedback">
				<label class="control-label clearfix">Título. </label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-pencil"></i></span>
					<input type="text" id="titulopregunta" name="titulopregunta" value="{{old('titulopregunta')}}" class="form-control" placeholder="INGRESE EL NOMBRE DE SU PREGUNTA."/>
				</div>
				<small id="errormessage_name" class="control-label clearfix pull-left"></small>
			</div>
            <div id="bloqueestado" class="col-xs-12 col-sm-6 col-md-6 has-feedback">
				<label class="control-label clearfix">Estilo. </label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-th"></i></span>
					<select class="form-control show-tick" id="tipopregunta" name="tipopregunta">
                        <option value="{{base64_encode('2')}}" selected>Seleccione el estilo de su pregunta</option>
						<option value="{{base64_encode('0')}}">Texto & Imagen</option>
						<option value="{{base64_encode('1')}}">Imagen</option>
					</select>
				</div>
				<small id="errormessage_estado" class="control-label clearfix pull-left"></small>
			</div>
        </div>
        <div id="" class="form-group">
            <div id="bloquepicture" class="col-xs-12 col-sm-12 col-md-12 has-feedback">
				<label class="control-label clearfix">Archivo Media</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-picture-o"></i></span>
					<input disabled type="file" id="imagenreferencial" name="imagenreferencial" value="{{old('imagenreferencial')}}" class="form-control" placeholder="INGRESE LA IMAGEN DE SU CURSO." onChange="openFile(event)"/>
				</div>
				<small id="errormessage_picture" class="control-label clearfix pull-left"></small>
			</div>
		</div>
        <div class="form-group">
            <div id="bloquedescripcion" class="col-xs-12 col-sm-12 col-md-12 has-feedback">
                <div id="descripcion"></div>
            </div>
		</div>
        <hr class="hr-cursos"/>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 botonera">
                @if(count($list)==0)
                    <button id="btnGuardar" type="button" class="btn btn-default pull-left" disabled>
                        <i class="fa fa-save" aria-hidden="true"></i> GUARDAR
                    </button>
                    <a id="btnExamen" href="{{url('course/course')}}" class="btn btn-default pull-left" disabled>
                        <i class="fa fa-book" aria-hidden="true"></i> EXAMEN
                    </a>
                @else
                    <button id="btnGuardar" type="button" class="btn btn-default pull-left">
                        <i class="fa fa-save" aria-hidden="true"></i> GUARDAR
                    </button>
                    <a id="btnExamen" href="{{url('course/course')}}" class="btn btn-default pull-left">
                        <i class="fa fa-book" aria-hidden="true"></i> EXAMEN
                    </a>
                    <a id="btnCancelar" href="#" class="btn btn-default pull-left">
                        <i class="fa fa-times" aria-hidden="true"></i> CANCELAR
                    </a>
                @endif
                <a id="btnRegresar" href="{{url('course/course')}}" class="btn btn-default pull-left">
                    <i class="fa fa-undo" aria-hidden="true"></i> REGRESAR
                </a>
                <a id="btnAdministrar" href="{{url('course/list')}}" class="btn btn-default pull-right">
                    <i class="fa fa-cog" aria-hidden="true"></i> ADMINISTRAR
                </a>
			</div>
		</div>
		<hr/>
    </form>
    @include("modales.alerta")
    @include("modales.wait")
	@include("modales.confirmar_courseasociated")
    @include("modales.confirmar_newquestions")
@endsection