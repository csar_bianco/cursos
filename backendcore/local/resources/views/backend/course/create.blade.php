@extends("layout.dashboard")
@section("mi-scripts")
<script src="{{asset('js/configurations.js')}}"></script>
<script src="{{asset('js/validaciones.js')}}"></script>
<script src="{{asset('js/modales.js')}}"></script>
<script src="{{asset('js/course/create.js')}}"></script>
@endsection
@section("section-body-course")
	@if(Session::has("resultado"))
		<input type="hidden" id="resultado" name="resultado" value="{{Session::get('resultado')}}"/>
	@endif
	<form id="form-cursos" class="form-horizontal" action="{{url('course/course')}}" accept-charset="UTF-8" enctype="multipart/form-data" method="post">
		<input type="hidden" id="_token" name="_token" value="{{csrf_token()}}"/>
		<input type="hidden" id="type" name="type" value="{{$type}}"/>
		<div id="bloquetexto" class="form-group">
			<div class="col-xs-12 col-md-12 is-empty text-center">
				@if($type=="0")
					<label for="">NOMBRE DE SU CURSO</label>
				@else
					<label for="">NOMBRE DE SU LECCIÓN</label>
				@endif
			</div>
		</div>
		<hr/>
		<div class="form-group">
			<div id="bloquecursonombre" class="col-xs-12 col-sm-12 col-md-12 has-feedback">
				<label class="control-label clearfix">Nombre. </label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-pencil"></i></span>
					@if($type=="0")
						<input type="text" id="nombrecurso" name="nombrecurso" value="{{old('nombrecurso')}}" class="form-control" placeholder="INGRESE EL NOMBRE DE SU CURSO."/>
					@else
						<input type="text" id="nombrecurso" name="nombrecurso" value="{{old('nombrecurso')}}" class="form-control" placeholder="INGRESE EL NOMBRE DE SU LECCIÓN."/>
					@endif
				</div>
				<small id="errormessage_name" class="control-label clearfix pull-left"></small>
			</div>
		</div>
		<div id="bloquedescripcion" class="form-group">
			<div class="col-xs-12 col-md-12 is-empty text-left">
				<label for="" class="">DATOS REFERENCIALES</label>
			</div>
		</div>
		<div class="form-group">
			<div id="bloquepicture" class="col-xs-12 col-sm-6 col-md-6 has-feedback">
				<label class="control-label clearfix">Imagen | Resolución: 128x128px</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-picture-o"></i></span>
					<input type="file" id="imagenreferencial" name="imagenreferencial" value="{{old('imagenreferencial')}}" class="form-control" placeholder="INGRESE LA IMAGEN DE SU CURSO." onChange="openFile(event)"/>
				</div>
				<small id="errormessage_picture" class="control-label clearfix pull-left"></small>
			</div>
			<div id="bloquefechainicio" class="col-xs-12 col-sm-6 col-md-6 has-feedback">
				<label class="control-label clearfix">Fecha Inicio. </label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					<input type="text" id="fechainicio" name="fechainicio" value="{{old('fechainicio')}}" class="form-control" placeholder="Formato: YYYY/MM/DD"/>
					<small id="errormessage_inicio" class="control-label clearfix pull-left"></small>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div id="bloquefechafin" class="col-xs-12 col-sm-6 col-md-6 has-feedback">
				<label class="control-label clearfix">Fecha Fin. </label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					<input type="text" id="fechafin" name="fechafin" value="{{old('fechafin')}}" class="form-control" placeholder="Formato: YYYY/MM/DD"/>
				</div>
			</div>
			<div id="bloqueestado" class="col-xs-12 col-sm-6 col-md-6 has-feedback">
				<label class="control-label clearfix">Estado. </label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-power-off"></i></span>
					<select class="form-control show-tick" id="estadocurso" name="estadocurso">
						@if($type=="0")
							<option value="{{base64_encode('2')}}" selected>Seleccione el estado de su curso</option>
						@else
							<option value="{{base64_encode('2')}}" selected>Seleccione el estado de su lección</option>
						@endif
						<option value="{{base64_encode('0')}}">Inactivo</option>
						<option value="{{base64_encode('1')}}">Activo</option>
					</select>
				</div>
				<small id="errormessage_estado" class="control-label clearfix pull-left"></small>
			</div>
		</div>
		<hr class="hr-cursos"/>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 botonera">
				@if($type=="0")
					<button id="btnGuardar" type="button" class="btn btn-default pull-left">
						<i class="fa fa-save" aria-hidden="true"></i> GUARDAR
					</button>
					<a id="btnLecciones" href="{{url('course/lessons')}}" class="btn btn-default pull-left">
						<i class="fa fa-plus" aria-hidden="true"></i> LECCIONES
					</a>
					<a id="btnLecciones" href="{{url('course/asignated')}}" class="btn btn-default pull-left">
						<i class="fa fa-users" aria-hidden="true"></i> ASIGNAR
					</a>
					<a id="btnLecciones" href="{{url('course/questions')}}" class="btn btn-default pull-left">
						<i class="fa fa-question" aria-hidden="true"></i> PPREGUNTAS
					</a>
					<a id="btnAdministrar" href="{{url('course/list')}}" class="btn btn-default pull-right">
						<i class="fa fa-cog" aria-hidden="true"></i> ADMINISTRAR
					</a>
				@else
					<button id="btnGuardar" type="button" class="btn btn-default pull-left">
						<i class="fa fa-save" aria-hidden="true"></i> GUARDAR
					</button>
					<a id="btnLecciones" href="{{url('course/course')}}" class="btn btn-default pull-left">
						<i class="fa fa-undo" aria-hidden="true"></i> REGRESAR
					</a>
					<a id="btnAdministrar" href="{{url('lessons/list')}}" class="btn btn-default pull-right">
						<i class="fa fa-cog" aria-hidden="true"></i> ADMINISTRAR
					</a>
				@endif
			</div>
		</div>
		<hr/>
	</form>
	@include("modales.alerta")
@endsection