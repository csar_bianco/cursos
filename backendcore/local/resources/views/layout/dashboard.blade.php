<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="csrf-token" content="{{ csrf_token() }}">
			<title>Cursos | Inicio</title>
			<!-- Tell the browser to be responsive to screen width -->
			<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
			<!-- Bootstrap 3.3.7 -->
			<link rel="stylesheet" href="{{asset('plugins/bootstrap/dist/css/bootstrap.min.css')}}">
			<!-- Font Awesome -->
			<link rel="stylesheet" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
			<!-- Jquery DataPicker -->
			<link rel="stylesheet" href="{{asset('plugins/datapicker/jquery-ui.min.css')}}">
			<link rel="stylesheet" href="{{asset('plugins/datapicker/jquery-ui.theme.min.css')}}">
			<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
			<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<![endif]-->
			<!-- style -->
			<link rel="stylesheet" href="{{asset('css/style.css')}}">
			<!-- Google Font -->
			<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
			<!-- Summernote -->
			<link rel="stylesheet" href="{{asset('plugins/summernote/dist/summernote.css')}}">
			<!-- LIGHT BOX -->
			<link href="{{asset('plugins/lightbox2-master/dist/css/lightbox.min.css')}}" rel="stylesheet">
			<!-- TABLA -->
			<link href="{{asset('plugins/DataTables-1.10.13/media/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
			<link href="{{asset('plugins/DataTables-1.10.13/extensions/FixedHeader/css/fixedHeader.dataTables.min.css')}}" rel="stylesheet">
			<link href="{{asset('plugins/DataTables-1.10.13/extensions/FixedHeader/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
			<link href="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
			<link href="{{asset('plugins/DataTables-1.10.13/extensions/Responsive/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
		</head>
		<body class="container-body-general">
			<section class="container-body">
				<div class="row">
					<div class="col-xs-12">
						@yield("section-body-course")
					</div>
				</div>
			</section>
			<!-- jQuery 3 -->
			<script src="{{asset('plugins/jquery/dist/jquery.min.js')}}"></script>
			<!-- Bootstrap 3.3.7 -->
			<script src="{{asset('plugins/bootstrap/dist/js/bootstrap.min.js')}}"></script>
			<!-- SlimScroll -->
			<script src="{{asset('plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
			<!-- FastClick -->
			<script src="{{asset('plugins/fastclick/lib/fastclick.js')}}"></script>
			<!-- Summernote -->
			<script src="{{asset('plugins/summernote/dist/summernote.js')}}"></script>
			<script src="{{asset('plugins/summernote/dist/es-ES.js')}}"></script>
			<!-- MASKED INPUT -->
			<script src="{{asset('plugins/masked/jquery.maskedinput.min.js')}}"></script>
			<!-- DataPicker -->
			<script src="{{asset('plugins/datapicker/jquery-ui.min.js')}}"></script>
			<script src="{{asset('plugins/datapicker/es.js')}}"></script>
			<!-- LIGHT BOX -->
			<script src="{{asset('plugins/lightbox2-master/dist/js/lightbox.min.js')}}"></script>
			<!-- TABLA -->
			<script src="{{asset('plugins/DataTables-1.10.13/media/js/jquery.dataTables.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/media/js/dataTables.bootstrap.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/FixedHeader/js/dataTables.fixedHeader.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/FixedHeader/js/dataTables.fixedHeader.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/js/buttons.bootstrap.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/js/buttons.html5.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/js/buttons.print.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Buttons/js/buttons.flash.min.js')}}"></script>
			<script src="{{asset('plugins/DataTables-1.10.13/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
			@yield("mi-scripts")
		</body>
	</html>