<div class="modal fade in" id="modal_confirmar_asociated" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content modal-border">
			<div class="modal-header"></div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-md-12 flex-center">
						CONFIRMAR
					</div>
					<hr class="hr-paraolimpico"/>
					<div class="col-xs-12 col-md-12">
						<span id="list_asociatedcourse" class="flex-center">
							
						</span>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button id="removerasociated" type="button" class="btn btn-link waves-effect" data-dismiss="modal">REMOVER</button>
				<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">SALIR</button>
			</div>
		</div>
	</div>
</div>