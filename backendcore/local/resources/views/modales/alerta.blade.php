<div class="modal fade in" id="modal_alerta" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog" role="document">
		<div class="modal-content modal-border">
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-md-12 flex-center">
						<strong id="title" class="flex-center">
							Advertencia
						</strong>
					</div>
				</div>
				<hr/>
				<div class="row">
					<div class="col-xs-12 col-md-12">
						<span id="advertenciamessage" class="flex-center">

						</span>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">SALIR</button>
			</div>
		</div>
	</div>
</div>