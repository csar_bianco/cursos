<div class="modal fade" id="modal_wait" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content modal-border modal-color">
			<div class="modal-header flex-center"></div>
			<div class="modal-body">
				<div class="row center">
					<div class="col-xs-12 col-md-12">
						<h4 id="waitformoment" class="text-center">Espere un momento.</h4>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-xs-12 col-md-12 flex-center">
						<i class="fa fa-spinner fa-spin fa-2x"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
