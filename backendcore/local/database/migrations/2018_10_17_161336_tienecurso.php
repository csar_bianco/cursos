<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tienecurso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('havecourse', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('course_parentid')->unsigned();
			$table->foreign('course_parentid')->references('id')->on('course');
			$table->integer('course_id')->unsigned();
			$table->foreign('course_id')->references('id')->on('course');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('havecourse');
    }
}
