<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PreguntasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		/*
			preguntas_codigo => CÓDIGO DE CADA PREGUNTA
			questions_body => CONTENIDO EN JSON DE LA ESTRUCTURA DE LA PREGUNTA
			questions_type => 0 => TEXTO & IMAGEN | 1 => VÍDEO | 2 => IMÁGEN
			questions_status => ESTADO DE PREGUNTA 0 => TEORÍA | 1 => PREGUNTA 
		*/
		
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
			$table->text('questions_body');
			$table->enum('questions_type',['0','1','2']);
			$table->enum('questions_status',['0','1']);
			$table->integer('course_parentid')->unsigned();
			$table->foreign('course_parentid')->references('id')->on('course');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
