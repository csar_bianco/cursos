<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CursoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		/*
			course_name => NOMBRE DEL CURSO
			course_route => IMAGEN DE SU CURSO
			course_date_init => FECHA DE INICIO DEL CURSO
			course_date_end => FECHA FIN DEL CURSO
			course_status => 0 => ACTIVO | 1 => NO ACTIVO
			course_type => 0 => CURSOS | 1 => LECCIONES
			course_level => PORCENTAJE DEL CURSO
			course_question => LA PREGUNTA ACTUAL EN LA QUE ESTÉ EL USUARIO
		*/
		
        Schema::create('course', function (Blueprint $table) {
            $table->increments('id');
			$table->string('course_name');
			$table->text('course_route');
			$table->string('course_date_init');
			$table->string('course_date_end');
			$table->enum('course_status',['0','1']);
			$table->enum('course_type',['0','1'])->default(0);
			$table->float('course_level');
			$table->integer('course_question');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course');
    }
}
