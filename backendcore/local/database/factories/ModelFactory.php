<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'user_name' => $faker->name,
        'user_email' => $faker->unique()->safeEmail,
        'user_password' => hash("md5",str_random(5)),
        'user_status' => 1,
        'user_role' => 1,
    ];
});
