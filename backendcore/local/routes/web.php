<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','DashboardController@course');
Route::group(["prefix"  => "course"], function () {
	//GET
	Route::get('course', ['uses' => 'DashboardController@course', 'as' => 'course/course']);
	Route::get('asociated', ['uses' => 'DashboardController@asociated', 'as' => 'course/asociated']);
	Route::get('asignated', ['uses' => 'DashboardController@asignated', 'as' => 'course/asignated']);
	Route::get('lessons', ['uses' => 'DashboardController@lessons', 'as' => 'course/lessons']);
	Route::get('questions', ['uses' => 'DashboardController@questions', 'as' => 'course/questions']);
	Route::get('list', ['uses' => 'DashboardController@listcourse', 'as' => 'course/list']);
	Route::get('list/users', ['uses' => 'DashboardController@listcourse', 'as' => 'course/list/users']);
	//POST
	Route::post('course', ['uses' => 'CursosController@course', 'as' => 'course/course']);
	Route::post('viewcourseasociated', ['uses' => 'CursosController@viewcourseasociated', 'as' => 'course/viewcourseasociated']);
	Route::post('asociated', ['uses' => 'CursosController@asociated', 'as' => 'course/asociated']);
	Route::post('asignated', ['uses' => 'CursosController@asignated', 'as' => 'course/asignated']);
	Route::post('questions', ['uses' => 'CursosController@questions', 'as' => 'course/questions']);
	//DELETE
	Route::post('delete', ['uses' => 'CursosController@deletecourse', 'as' => 'course/delete']);
});

Route::group(["prefix"  => "lessons"], function () {
	//GET
	Route::get('list', ['uses' => 'DashboardController@listlessons', 'as' => 'lessons/list']);
	//DELETE
	Route::post('delete', ['uses' => 'CursosController@deletecourse', 'as' => 'lessons/delete']);
});

Route::group(["prefix"  => "questions"], function () {
});