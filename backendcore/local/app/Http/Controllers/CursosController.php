<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Questions;
use Session;

class CursosController extends Controller
{
    //STORE
    public function course(Request $request){
		$state = Course::store_course($request->all());
        if($state[1]==200){
            Session::flash("resultado",$state[0]);
            if($request->all()["type"]==0)
                return redirect()->to("course/course");
            else
                return redirect()->to("course/lessons");
        }else{
            Session::flash("resultado",$state[0]);
            return redirect()->back()->withInput();
        }
	}
	
	public function asociated(Request $request){
		$state = Course::asociated($request->all());
        if($state[1]==200){
            Session::flash("resultado",$state[0]);
            return redirect()->to("course/asociated");
        }else{
            Session::flash("resultado",$state[0]);
            return redirect()->back()->withInput();
        }
	}
    
    public function asignated(Request $request){
        $state = Course::asignated($request->all());
        if($state[1]==200){
            Session::flash("resultado",$state[0]);
            return redirect()->to("course/asignated");
        }else{
            Session::flash("resultado",$state[0]);
            return redirect()->back()->withInput();
        }
    }

    public function questions(Request $request){
        $state = Questions::questions($request->all());
        return response()->json([
            "response" => $state[0],
            "code" => $state[1]
        ],$state[1]);
    }

    //VIEW
    public function viewcourseasociated(Request $request){
        $state = Course::viewcourseasociated($request->all());
        return response()->json([
			"response" => $state[0],
			"code" => $state[1]
		],$state[1]);
    }

    //DELETE
	public function deletecourse(Request $request){
        $state = Course::deletecourse($request->all());
		return response()->json([
			"response" => $state[0],
			"code" => $state[1]
		],$state[1]);
	}
}
