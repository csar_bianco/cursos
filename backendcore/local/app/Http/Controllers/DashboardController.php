<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\User;

class DashboardController extends Controller
{
    
	//STORE
	public function course(){
		return view("backend.course.create",[
			"type" => '0'
		]);
	}
	
	public function lessons(){
		return view("backend.course.create",[
			"type" => '1'
		]);
	}

	public function asignated(){
		return view("backend.course.asignated",[
			"list" => json_decode(Course::list("0")),
			"users" => json_decode(User::list())
		]);
	}

	public function questions(){
		return view("backend.course.questions",[
			"list" => json_decode(Course::list("0")),
		]);
	}
	
	//ACTUALIZAR
	public function asociated(){
		$lista = json_decode(Course::list("1"));
		return view("backend.course.asociated",[
			"list" => $lista,
			"course" => json_decode(Course::list("0"))
		]);
	}
	
	//LISTAR
	public function listcourse(){
		$list = json_decode(Course::list("0"));
		return view("backend.course.list",[
			"list" => $list,
			"type" => "0"
		]);
	}
	
	public function listlessons(){
		$lista = json_decode(Course::list("1"));
		return view("backend.course.list",[
			"list" => $lista,
			"type" => "1"
		]);
	}
}
