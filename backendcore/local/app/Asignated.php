<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asignated extends Model
{
    protected $table = "asignated";
	
    protected $fillable = [
        'id',
        'user_id',
		'course_id',
    ];
    
    //VERIFICO QUE EL USUARIO NO POSEA MÁS DE UN CURSO ASIGNADO
    public static function verifycourse($courseid){
        $course = Asignated::where("course_id",base64_decode($courseid));
		if($course!=NULL && $course->first()!=NULL){
			return 1;
		}
		
		return 0;
    }

    //ASIGNO UN(OS) CURSO(S) A UN USUARIO
    public static function asignated($inputs){
        $asignations = json_decode($inputs["asociados"]);
        foreach($asignations as $l){
            $elements = json_decode($l,true);
            Asignated::create([
                'user_id' => base64_decode($elements['user']),
		        'course_id' => base64_decode($elements['course']),
            ]);
        }
    }
}