<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Lang;
use App\HaveCourse;
use App\Asignated;

class Course extends Model
{
    protected $table = "course";
	
    protected $fillable = [
        'id',
        'course_name',
		'course_route',
		'course_date_init',
		'course_date_end',
		'course_status',
		'course_type',
		'course_level',
		'course_question'
	];
	
    protected $hidden = [];
	
	public function course(){
        return $this->belongsToMany('App\Course');
	}

	public function user(){
        return $this->belongsToMany('App\User');
	}

	public function havecourse(){
        return $this->belongsToMany('App\Course');
	}
	
	//SI EL NOMBRE EXISTE RETORNO SU ID Y TRUE
	private static function verifyname($name){
		$course = json_decode(Course::get()->toJson(),true);
		foreach($course as $c){
			if(strtoupper($c['course_name'])==$name){
				return [1,$c['id']];
			}
		}
		
		return [0,NULL];
	}
	
	//GUARDO UN CURSO EN EL SISTEMA
	public static function store_course($inputs){
		if(array_key_exists("nombrecurso",$inputs) && $inputs["nombrecurso"]!="" && array_key_exists("fechainicio",$inputs) && $inputs["fechainicio"]!=""){
			if(array_key_exists("fechafin",$inputs) && $inputs["fechafin"]!="" && array_key_exists("estadocurso",$inputs) && $inputs["estadocurso"]!=""){
				if(array_key_exists("imagenreferencial",$inputs) && $inputs["imagenreferencial"]!=""){
					$state = Course::verifyname(strtoupper($inputs["nombrecurso"]));
					if($state[0]==0){
						//SETUP IMAGEN
						$file = $_FILES["imagenreferencial"]["name"];
						$ext = substr($file,strrpos($file,"."),strlen($file));
						$media = hash("md5","course")."/".md5(str_random(5).$_FILES["imagenreferencial"]["name"]).$ext;
						\Storage::disk('media')->put($media,  \File::get($inputs["imagenreferencial"]));
						$media = ("img/media/".$media);
						Course::create([
							'course_name' => $inputs["nombrecurso"],
							'course_route' => $media,
							'course_date_init' => $inputs["fechainicio"],
							'course_date_end' => $inputs["fechafin"],
							'course_status' => base64_decode($inputs["estadocurso"]),
							'course_level' => 0,
							'course_question' => 0,
							'course_type' => $inputs['type']
						]);
						
						return [Lang::get("message.yes_register"),200];
					}else{
						return [Lang::get("error.match_name"),409];
					}
				}
			}
		}
		
		return [Lang::get("error.general_error"),500];
	}
	
	//ASOCIO UN CURSO/LECCIÓN CON UN CURSO
	public static function asociated($inputs){
		if(array_key_exists("asociados",$inputs) && $inputs["asociados"]!=""){
			$asociated = json_decode(json_decode($inputs['asociados'],true)[0],true);
			$state = HaveCourse::verifycourse($asociated["parents"]);
			if(!$state){
				HaveCourse::asociated($inputs);
					
				return [Lang::get("message.yes_asociated"),200];
			}else{
				return [Lang::get("error.match_asociated"),409];
			}
		}
		
		return [Lang::get("error.general_error"),500];
	}
	
	//ASIGNO UN CURSO A UN USUARIO: SI UN CURSO SE ENCUENTRA PREVIAMENTE ASIGNADO, NO CONCLUYE LAS ASIGNACIONES POR REGISTRAR.
	public static function asignated($inputs){
		if(array_key_exists("asociados",$inputs) && $inputs["asociados"]!=""){
			$asociated = json_decode($inputs['asociados'],true);
			$exits = 0;
			foreach($asociated as $a){
				$elements = json_decode($a,true);
				$state = Asignated::verifycourse($elements['course']);
				if($state==1){
					$exits = 1;
					break;
				}
			}

			if(!$exits){
				Asignated::asignated($inputs);
				return [Lang::get("message.yes_asignated"),200];
			}else{
				return [Lang::get("error.match_asignated_user"),409];
			}
		}

		return [Lang::get("error.general_error"),500];
	}

	//REMOVER UN CURSO
	public static function deletecourse($inputs){
		$state = HaveCourse::verifycourse($inputs["codigo"]);
		if(!$state){
			$course = Course::where("id",base64_decode($inputs["codigo"]));
			if($course!=NULL && $course->first()!=NULL){
				if(file_exists($course->first()->course_route)){
					unlink($course->first()->course_route);
				}
				
				$course->delete();
				return [Lang::get("message.yes_delete_curso"),200];
			}
		}else{
			return [Lang::get("error.match_course_delete"),409];
		}

		return [Lang::get("error.general_error"),500];
	}
	
	//VIEW
	public static function viewcourseasociated($inputs){
		$course = json_decode(Course::courseAsociated(base64_decode($inputs["codigo"])),true);
		$array = [];
		$i = 1;
		if(count($course)==0){
			return [Lang::get("error.not_course_asociated"),404];
		}else{
			$array[0] = Course::find($course[0]['course_parentid']);
			foreach($course as $c){
				$array[$i++] = Course::find($c['course_id']);			
			}

			return [$array,200];
		}
	}

	//LISTAR POR EL TIPO LOS CURSOS
	public static function list($tipo){
		$course = Course::where("course_type",$tipo)->get()->toJson();
		return $course;
	}

	//LISTA LOS CURSOS ASOCIADOS A UNA LECCiÓN
	public static function courseAsociated($idcourse){
		$course = Course::join("havecourse","havecourse.course_parentid","=","course.id")->where("course.id","=",$idcourse)->get()->toJson();
		return $course;
	}
}