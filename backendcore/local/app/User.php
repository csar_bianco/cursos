<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = "user";
	
    protected $fillable = [
        'id',
        'users_name',
		'users_email',
		'users_password',
		'users_status',
		'users_role',
    ];

    public function course(){
        return $this->belongsToMany('App\Course');
    }
    
    //LISTAR TODOS LOS USUARIOS
	public static function list(){
		$users = User::get()->toJson();
		return $users;
	}
}
