<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HaveCourse extends Model
{
    protected $table = "havecourse";
	
    protected $fillable = [
        'id',
        'course_parentid',
		'course_id',
	];
	
	public static function verifycourse($courseid){
		$course = HaveCourse::where("course_parentid",base64_decode($courseid));
		if($course!=NULL && $course->first()!=NULL){
			return 1;
		}
		
		return 0;
	}

	public static function asociated($inputs){
		$lessons = json_decode($inputs["asociados"]);
		foreach($lessons as $l){
			$elements = json_decode($l,true);
			HaveCourse::create([
				'course_parentid' => base64_decode($elements['parents']),
				'course_id' => base64_decode($elements['child']),
			]);
		}
	}
}