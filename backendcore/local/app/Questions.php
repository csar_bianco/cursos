<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    protected $table = "questions";
	
    protected $fillable = [
        'id',
        'questions_body',
        'questions_type',
        'questions_status',
        'course_parentid'
    ];
    
    public static function questions($inputs){
        return [$inputs,500];
    }
	
}
